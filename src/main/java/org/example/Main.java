package org.example;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) throws DivisionException {
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(Main.class.getPackageName());

               Addition addition = applicationContext.getBean(Addition.class);
               Subtraction subtraction = applicationContext.getBean(Subtraction.class);
               Division division = applicationContext.getBean(Division.class);
               Multiplication multiplication = applicationContext.getBean(Multiplication.class);
        System.out.println(addition.operation(245, 768));
        System.out.println(subtraction.operation(435, 76));
        System.out.println(division.operation(55,5));
        System.out.println(multiplication.operation(65,4));
    }
}