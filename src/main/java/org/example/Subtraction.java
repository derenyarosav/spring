package org.example;

import org.springframework.stereotype.Component;

@Component
public class Subtraction implements Action{

    @Override
    public double operation(double num1, double num2) {
        return num1 - num2;
    }
}
