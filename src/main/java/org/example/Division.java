package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Division implements Action{
    @Autowired
    private Division division;

    @Override
    public double operation(double num1, double num2) throws DivisionException {
      if (num1 == 0){
          throw new DivisionException("Не можна ділити на 0!");
      } else if (num2 == 0) {
          throw new DivisionException("Не можна ділити на 0!");
      }
      else
        return num1 / num2;
    }
}
