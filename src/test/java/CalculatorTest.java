import org.example.*;
import org.junit.Test;
import org.springframework.stereotype.Component;

import static org.junit.Assert.assertEquals;

@Component
public class CalculatorTest {
    Division division = new Division();
    Addition addition = new Addition();
    Subtraction subtraction = new Subtraction();
    Multiplication multiplication = new Multiplication();

    @Test(expected = DivisionException.class)
    public void verifyTheCorrectnessOfDivisionNullException() throws DivisionException {
        division.operation(55,0);
    }
        @Test
        public void verifyTheCorrectnessOfDivision () throws DivisionException {
            assertEquals(11, division.operation(55, 5), 0);
        }

        @Test
        public void verifyTheCorrectnessOfAddition () {
            assertEquals(1013, addition.operation(245, 768), 0);
        }
        @Test
        public void verifyTheCorrectnessOfSubtraction () {
            assertEquals(359, subtraction.operation(435, 76), 0);
        }
        @Test
        public void verifyTheCorrectnessOfMultiplication () {
            assertEquals(260, multiplication.operation(65, 4), 0);

    }
}